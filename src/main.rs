extern crate chrono;

use chrono::prelude::*;
use std::env;
use std::thread::spawn;

#[derive(PartialEq, PartialOrd, Debug, Clone)]
pub struct Pi {
    i: f64,
    s: f64,
}

impl Pi {
    pub fn new() -> Pi {
        Pi { i: 1.0, s: 0.0 }
    }
}

impl Iterator for Pi {
    type Item = f64;
    fn next(&mut self) -> Option<f64> {
        self.s += 1.0/(self.i*(self.i+2.0));
        self.i += 4.0;
        Some(self.s*8.0)
    }
}

pub fn compute_pi(b: usize) -> f64 {
    Pi::new().nth(b).unwrap()
}

fn main() {
    let argv: Vec<String> = env::args().collect();
    let n = argv.get(1).map(|v| v.parse().unwrap_or(8)).unwrap_or(8);
    let b = argv.get(2).map(|v| v.parse().unwrap_or(1.0e9 / 4.0)).unwrap_or(1.0e9 / 4.0) as usize;
    let r = argv.get(3).map(|v| v.parse().unwrap_or(100)).unwrap_or(100);
    let mut ts = vec![];
    let st = Utc::now();
    for _ in 0..n {
        ts.push(spawn(move || {
            let mut res = vec![];
            for _ in 0..r {
                res.push(compute_pi(b));
            }
            res
        }));
    }
    let mut rs = vec![];
    for t in ts {
        rs.push(t.join());
    }
    let et = Utc::now();
    let tt = et-st;
    println!("Computing `compute_pi({}), {} times, on each of {} threads took {} or {} pims", b, r, n, tt, ((b as f64) * (r as f64) * (n as f64)) / (tt.num_milliseconds() as f64));
    for r in rs {
        match r {
            Ok(_) => (),
            Err(e) => panic!(e),
        }
    }
}
